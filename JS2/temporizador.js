var totalSegundos;
var interval;
var horasRestantes;
var minutosRestantes;
var segundosRestantes;

function iniciarCuentaRegresiva() {
	var horas = document.getElementById("horas").value;
	var minutos = document.getElementById("minutos").value;
	var segundos = document.getElementById("segundos").value;

	if (horas == "" || minutos == "" || segundos == "") {
		alert("Introduce un tiempo válido.");
		return;
	}

	totalSegundos = parseInt(horas)*3600 + parseInt(minutos)*60 + parseInt(segundos);

	interval = setInterval(cuentaRegresiva, 1000);
}

function pausarCuentaRegresiva() {
	clearInterval(interval);
}

function reanudarCuentaRegresiva() {
	interval = setInterval(cuentaRegresiva, 1000);
}

function reiniciarCuentaRegresiva() {
	clearInterval(interval);
	document.getElementById("horas").value = "";
	document.getElementById("minutos").value = "";
	document.getElementById("segundos").value = "";
	document.getElementById("cuentaRegresiva").innerHTML = "00:00:00";
}

function cuentaRegresiva() {
	horasRestantes = Math.floor(totalSegundos / 3600);
	minutosRestantes = Math.floor((totalSegundos - horasRestantes*3600) / 60);
	segundosRestantes = totalSegundos % 60;

	if (horasRestantes < 10) {
		horasRestantes = "0" + horasRestantes;
	}

	if (minutosRestantes < 10) {
		minutosRestantes = "0" + minutosRestantes;
	}

	if (segundosRestantes < 10) {
		segundosRestantes = "0" + segundosRestantes;
	}

	document.getElementById("cuentaRegresiva").innerHTML = horasRestantes + ":" + minutosRestantes + ":" + segundosRestantes;

	totalSegundos--;

	if (totalSegundos < 0) {
		clearInterval(interval);
		document.getElementById("cuentaRegresiva").innerHTML = "¡Tiempo terminado!";
	}
}
