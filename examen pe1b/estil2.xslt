<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>
        <h2>Plantes que floreixen al estiu:</h2>
        <table>
          <tr>
            <th>Nom</th>
            <th>Especie</th>
            <th>Color</th>
            <th>Temps de floracio</th>
          </tr>
          <xsl:for-each select="garden/planta[temps_floracio='Estiu']">
            <tr>
              <td><xsl:value-of select="nom"/></td>
              <td><xsl:value-of select="especie"/></td>
              <td><xsl:value-of select="color"/></td>
              <td><xsl:value-of select="temps_floracio"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
  </xsl:stylesheet>
