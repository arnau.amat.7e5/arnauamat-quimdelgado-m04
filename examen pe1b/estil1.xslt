<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
        <h2>Plantes que floreixen a l'estiu:</h2>
        <xsl:apply-templates select="//planta[temps_floracio='Estiu']">
          <xsl:sort select="nom" />
        </xsl:apply-templates>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="planta">
    <p>
      <xsl:number value="position()" />
      <xsl:text>. </xsl:text>
      <xsl:value-of select="nom" />
      <xsl:text>&#xa0;&#xa0;&#xa0;</xsl:text>
      <xsl:value-of select="especie" />
      <xsl:text>&#xa0;&#xa0;&#xa0;</xsl:text>
      <xsl:value-of select="color" />
      <xsl:text>&#xa0;&#xa0;&#xa0;</xsl:text>
      <xsl:value-of select="temps_floracio" />
    </p>
  </xsl:template>

</xsl:stylesheet>
